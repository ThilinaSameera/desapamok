<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {

 public function insert_student_data($data){
     $this->db->set('joined_date', 'NOW()', FALSE);
     $this->db->insert('students', $data);
     return $this->db->insert_id();
 }


 public function otp_validate($otp,$username){
    $this->db->select('OTP');
    $this->db->from('students');
    $this->db->where('students.OTP',$otp);
    $this->db->where('students.mobile',$username);
    $query = $this->db->get();
    $result = $query->row_array();
    if($result['OTP'] == $otp){
        $data['is_active']= ACTIVE;
        $this->db->where('mobile', $username);
        $this->db->update('students', $data);
        return true;
    }else{
        return false;
    }

 }

 public function select_student_by_username($username,$password){
    $this->db->select();
    $this->db->from('students');
    $this->db->where('password',$password);
    $query =$this->db->get();
    return $query->row_array();
 }


    

}