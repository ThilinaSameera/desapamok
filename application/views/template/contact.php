<div class="section section-contact fp-auto-height-responsive no-slide-arrows " data-section="contact">

  <!-- begin of information slide -->
  <div class="slide" id="information" data-anchor="information">
    <!-- Begin of slide section wrapper -->
    <div class="section-wrapper">
      <!-- title -->
      <div class="section-title text-center">
        <h5 class="title-bg">Contact</h5>
      </div>

      <div class="row">
        <div class="col-12 col-md-6">
          <!-- content -->
          <div class="section-content anim text-left">
            <!-- title and description -->
            <div class="title-desc">
              <div class="anim-2">
                <h5>Customer Service</h5>
                <h2 class="display-4 display-title">Contact</h2>
                <p>For questions about our company and products found on our stores.
                  Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis
                  dui. Fusce in urna sem.</p>
              </div>
              <div class="address-container anim-3">

                <div class="row">
                  <div class="col-12 col-md-12 col-lg-6">
                    <h4>Contact</h4>
                    <p>Call: 01 234 567 89</p>
                    <p>Email: ouremail@domain.com</p>
                  </div>
                  <div class="col-12 col-md-12 col-lg-6">
                    <h4>Address</h4>
                    <p>
                      Company address
                      <br>12 Street Turning Place
                      <br>South Est, Antartica
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <!-- Action button -->
            <div class="btns-action anim-4">
              <a class="btn btn-outline-white btn-round" href="#contact/message">
                <span class="txt">Send Message</span>
              </a>
            </div>
          </div>
        </div>

        <div class="col-12 col-md-6">

        </div>
      </div>
    </div>
    <!-- End of slide section wrapper -->
  </div>
  <!-- end of information slide -->

  <!-- begin of message slide -->
  <div class="slide" id="message" data-anchor="message">
    <!-- Begin of slide section wrapper -->
    <div class="section-wrapper">
      <div class="row justify-content-between">
        <div class="col-12 col-md-6 center-vh">
          <!-- content -->
          <div class="section-content anim text-left">
            <!-- title and description -->
            <div class="title-desc">
              <div>
                <h5>Customer Service</h5>
                <h2 class="display-4 display-title">Email Us</h2>
                <p>For questions about our company and products found on our stores.
                  Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis
                  dui. Fusce in urna sem.</p>
              </div>
            </div>

            <!-- Action button -->
            <div class="btns-action">
              <a class="btn btn-outline-white btn-round" href="#contact/information">
                <span class="txt">Information</span>
              </a>
            </div>
          </div>
        </div>

        <div class="col-12 col-md-6 col-lg-5">
          <!-- content -->
          <div class="section-content anim text-left">
            <!-- title and description -->
            <div class="">
              <div class="form-container form-container-card">
                <!-- Message form container -->
                <form class="send_message_form message form" method="post" action="ajaxserver/serverfile.php"
                id="message_form">
                  <div class="form-group name">
                    <label for="mes-name">Name :</label>
                    <input id="mes-name" name="name" type="text" placeholder="" class="form-control-line form-success-clean"
                    required>
                  </div>
                  <div class="form-group email">
                    <label for="mes-email">Email :</label>
                    <input id="mes-email" type="email" placeholder="" name="email" class="form-control-line form-success-clean"
                    required>
                  </div>
                  <div class="form-group no-border">
                    <label for="mes-text">Message</label>
                    <textarea id="mes-text" placeholder="..." name="message" class="form-control form-control-outline thick form-success-clean"
                    required></textarea>

                    <div>
                      <p class="message-ok invisible form-text-feedback form-success-visible">Your message has been sent, thank you.</p>
                    </div>
                  </div>

                  <div class="btns">
                    <button id="submit-message" class="btn btn-normal btn-white btn-round btn-full email_b"
                    name="submit_message">
                      <span class="txt">Send</span>
                      <span class="arrow-icon"></span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End of slide section wrapper -->
  </div>
  <!-- end of message slide -->

</div>