 <div class="section section-description fp-auto-height-responsive " data-section="about">
      <!-- Begin of section wrapper -->
      <div class="section-wrapper center-vh dir-col anim">
        <!-- title -->
        <div class="section-title text-center">
          <h5 class="title-bg">About</h5>
          <h2 class="display-4 display-title anim-2">Modern Website for your business</h2>
        </div>

        <!-- content -->
        <div class="section-content reduced anim text-center">
          <!-- title and description -->
          <div class="title-desc anim-3">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet
              malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum
              tincidunt non. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis
              dui. Fusce in urna sem.</p>
          </div>

        </div>

        <!-- Arrows scroll down/up -->
        <footer class="section-footer scrolldown">
          <a class="down">
            <span class="icon"></span>
            <span class="txt">Team</span>
          </a>
        </footer>
      </div>
      <!-- End of section wrapper -->
    </div>
    <!-- End of description section -->

    <!-- Begin of description section -->
    <div class="section section-description fp-auto-height-responsive " data-section="team">
      <!-- Begin of section wrapper -->
      <div class="section-wrapper center-vh dir-col anim">
        <!-- title -->
        <div class="section-title text-center">
          <h5 class="title-bg">Team</h5>
        </div>

        <!-- content -->
        <div class="section-content anim text-center">
          <!-- text or illustration order are manipulated via Bootstrap order-md-1, order-md-2 class -->
          <!-- begin of item -->
          <div class="item row justify-content-between">
            <!-- img-frame-normal demo -->
            <div class="col-12 col-sm-6 col-md-4 center-vh">
              <div class="section-content anim translateUp">
                <div class="images text-center">
                  <div class="img-avatar-alpha">
                    <div class="img-1 shadow">
                      <a href="item.html">
                        <img class="img" src="<?php echo base_url('assets/img/items/img-people1-square.jpg')?>" alt="Image">
                      </a>
                    </div>
                    <div class="legend text-center pos-abs">
                      <h5>Flavien Dupon</h5>
                      <p class="small">Web developer / Designer</p>
                      <div class="icons">
                        <a class="icon-btn" href="#">
                          <i class="icon fa fa-instagram"></i>
                        </a>
                        <a class="icon-btn" href="#">
                          <i class="icon fa fa-twitter"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- img-frame-normal demo -->
            <div class="col-12 col-sm-6 col-md-4 center-vh">
              <div class="section-content anim">
                <div class="images text-center">
                  <div class="img-avatar-alpha">
                    <div class="img-1 shadow">
                      <a href="item.html">
                        <img class="img" src="<?php echo base_url('assets/img/items/img-people2-square.jpg')?>" alt="Image">
                      </a>
                    </div>
                    <div class="legend text-center pos-abs">
                      <h5>Geraldine Cyclone</h5>
                      <p class="small">Architect / Photograph</p>
                      <div class="icons">
                        <a class="icon-btn" href="#">
                          <i class="icon fa fa-instagram"></i>
                        </a>
                        <a class="icon-btn" href="#">
                          <i class="icon fa fa-twitter"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- img-frame-normal demo -->
            <div class="col-12 col-sm-6 col-md-4 center-vh">
              <div class="section-content anim translateDown">
                <div class="images text-center">
                  <div class="img-avatar-alpha">
                    <div class="img-1 shadow">
                      <a href="item.html">
                        <img class="img" src="<?php echo base_url('assets/img/items/img-people3-square.jpg')?>" alt="Image">
                      </a>
                    </div>
                    <div class="legend text-center pos-abs">
                      <h5>Tintin Hergé</h5>
                      <p class="small">Co-founder / Blogger</p>
                      <div class="icons">
                        <a class="icon-btn" href="#">
                          <i class="icon fa fa-instagram"></i>
                        </a>
                        <a class="icon-btn" href="#">
                          <i class="icon fa fa-twitter"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- end of item -->
        </div>

        <!-- Arrows scroll down/up -->
        <footer class="section-footer scrolldown">
          <a class="down">
            <span class="icon"></span>
            <span class="txt">Our Services</span>
          </a>
        </footer>
      </div>
      <!-- End of section wrapper -->
    </div>