<div class="section section-register fp-auto-height-responsive " data-section="register">
  <!-- Begin of section wrapper -->
  <div class="section-wrapper">
    <!-- title -->
    <div class="section-title text-center">
      <h5 class="title-bg">Subscribe</h5>
    </div>

    <!-- content -->
    <div class="section-content anim text-center">

      <div class="row align-items-center justify-content-center">

        <div class="col-12 col-md-8 col-lg-6">
          <!-- Registration form container-->
          <form class="send_email_form form-container form-container-transparent form-container-white"
          method="post" action="ajaxserver/serverfile.php">
            <div class="form-desc">
              <h2 class="display-4 display-title  anim-2">Subscribe</h2>
              <p class="invite  anim-3">Don't miss any new opportunity, Hurry up! register now</p>
            </div>
            <div class="form-input  anim-4">
              <div class="form-group form-success-gone">
                <label for="reg-name">Name</label>
                <input id="reg-name" name="name" class="form-control-line form-control-white" type="text"
                />
              </div>
              <div class="form-group form-success-gone">
                <label for="reg-email">Email</label>
                <input id="reg-email" name="email" class="form-control-line form-control-white" type="email"
                required placeholder="your@email.address" data-validation-type="email"
                />
              </div>
              <div class="form-group mb-0">
                <div>
                  <p class="email-ok invisible form-text-feedback form-success-visible">Your email has been registred, thank you.</p>
                </div>
                <button id="submit-email" class="btn btn-white btn-round btn-full form-success-gone"
                name="submit_email">Subscribe</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- End of section wrapper -->
</div>