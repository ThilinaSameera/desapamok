<header class="page-header navbar page-header-alpha scrolled-white menu-right topmenu-right">

    <!-- Begin of menu icon toggler -->
  <!--   <button class="navbar-toggler site-menu-icon" id="navMenuIcon">
    
      <span class="menu-icon menu-icon-random">
        <span class="bars">
          <span class="bar1"></span>
          <span class="bar2"></span>
          <span class="bar3"></span>
        </span>
      </span>
    </button> -->
    <!-- End of menu icon toggler -->

    <!-- Begin of logo/brand -->
    <a class="navbar-brand" href="#">
      <span class="logo">
        <img class="light-logo" src="<?php echo base_url('assets/img/disapamok.jpg')?>" alt="Logo">
      </span>
      <span class="text">
        <!-- <span class="line">isapamok</span> -->
        <span class="line sub">Portfolio Template</span>
      </span>
    </a>
    <!-- End of logo/brand -->

    <!-- begin of menu wrapper -->
    <!-- <div class="all-menu-wrapper" id="navbarMenu"> -->
      <!-- Begin of top menu -->
      <!-- <nav class="navbar-topmenu"> -->
        <!-- Begin of CTA Actions, & Icons menu -->
        <!-- <ul class="navbar-nav navbar-nav-actions">
          <li class="nav-item">
            <a class="btn btn-outline-white btn-round" href="#register_modal1" data-toggle="modal" target="_blank">
             Register
            </a>
          </li> <li class="nav-item"> -->
            <?php if($this->session->userdata('USER_TYPE')== STUDENT) {?>
              <a class="btn btn-outline-white btn-round"  href="#">
                Logout
              </a>
            <?php }else{?>
            <!-- <a class="btn btn-outline-white btn-round" target="_blank" data-toggle="modal" href="#login_modal">
              Login
            </a> -->
          <?php }?>
          <!-- </li>
        </ul> -->
        <!-- End of CTA & Icons menu -->
      <!-- </nav> -->
      <!-- End of top menu -->

      <!-- Begin of hamburger mainmenu menu -->
    <!--   <nav class="navbar-mainmenu">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="./index.html#home">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./gallery.html">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./item.html">Item</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./demo.html">Demo</a>
          </li>
        </ul>
      </nav> -->
      <!-- End of hamburger mainmenu menu -->

      <!-- Begin of sidebar nav menu params class: text-only / icon-only-->
      <nav class="navbar-sidebar ">
        <ul class="navbar-nav custom-navbar" id="qmenu">
          <li class="nav-item custom-navbar" data-menuanchor="home">
            <a href="#home">
              <i class="icon ion-ios-home-outline"></i>
              <span class="txt">Home &nbsp;</span>
            </a>
          </li>
          <li class="nav-item custom-navbar" data-menuanchor="about">
            <a href="#about">
              <i class="icon ion-ios-information-outline"></i>
              <span class="txt">About &nbsp;</span>
            </a>
          </li>
          <li class="nav-item custom-navbar" data-menuanchor="services">
            <a href="#services">
              <i class="icon ion-ios-list-outline"></i>
              <span class="txt">Subjects &nbsp;</span>
            </a>
          </li>
          <li class="nav-item custom-navbar" data-menuanchor="projects">
            <a href="#projects">
              <i class="icon ion-ios-albums-outline"></i>
              <span class="txt">Projects &nbsp;</span>
            </a>
          </li>
          <li class="nav-item custom-navbar" data-menuanchor="register">
            <a href="#register">
              <i class="icon ion-ios-compose-outline"></i>
              <span class="txt">Register &nbsp;</span>
            </a>
          </li>
          <li class="nav-item custom-navbar" data-menuanchor="contact">
            <a href="#contact">
              <i class="icon ion-ios-telephone-outline"></i>
              <span class="txt">Contact &nbsp;</span>
            </a>
          </li>
        </ul>
      </nav>
      <!-- End of sidebar nav menu -->
    </div>
    <!-- end of menu wrapper -->

  </header>

  <style type="text/css">
    body {
    font-family: 'Varela Round', sans-serif;
  }
  .modal-login {    
    color: #636363;
    width: 350px;
  }
  .modal-login .modal-content {
    padding: 20px;
    border-radius: 5px;
    border: none;
  }
  .modal-login .modal-header {
    border-bottom: none;   
        position: relative;
        justify-content: center;
  }
  .modal-login h4 {
    text-align: center;
    font-size: 26px;
    margin: 30px 0 -15px;
  }
  .modal-login .form-control:focus {
    border-color: #70c5c0;
  }
  .modal-login .form-control, .modal-login .btn {
    min-height: 40px;
    border-radius: 3px; 
  }
  .modal-login .close {
        position: absolute;
    top: -5px;
    right: -5px;
  } 
  .modal-login .modal-footer {
    background: #ecf0f1;
    border-color: #dee4e7;
    text-align: center;
        justify-content: center;
    margin: 0 -20px -20px;
    border-radius: 5px;
    font-size: 13px;
  }
  .modal-login .modal-footer a {
    color: #999;
  }   
  .modal-login .avatar {
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: -70px;
    width: 95px;
    height: 95px;
    border-radius: 50%;
    z-index: 9;
    background: #fe0000;
    padding: 15px;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
  }
  .modal-login .avatar img {
    width: 100%;
  }
  .modal-login.modal-dialog {
    margin-top: 80px;
  }
    .modal-login .btn {
        color: #fff;
        border-radius: 4px;
    background: #fe0000;
    text-decoration: none;
    transition: all 0.4s;
        line-height: normal;
        border: none;
    }
  .modal-login .btn:hover, .modal-login .btn:focus {
    background: #f12200;
    outline: none;
  }
  .trigger-btn {
    display: inline-block;
    margin: 100px auto;
  }
</style>

<div id="login_modal" class="modal fade">
  <div style="width: 350px" class="modal-dialog modal-login">
    <div class="modal-content">
      <div class="modal-header">
        <div class="avatar">
          <img src="<?php echo base_url('assets/img/fevicon.jpg')?>" alt="Avatar">
        </div>        
        <h4 class="modal-title">Member Login</h4> 
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <a href="#" id="active_btn" class="" style="display: none;">Active Account</a>
          </div>
        <form action="" id="login_form" method="post">
          <div class="form-group">
            <input type="text" class="form-control" name="lgn_username" placeholder="Username" required="required" id="lgn_username">   
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="lgn_password" placeholder="Password" required="required"> 
          </div>        
          <div class="form-group">
            <button type="button" id="login_btn" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
          </div>
        </form>
        <form action="" id="otp_form_login" method="post" style="display: none;">
           <div class="form-group">
            <input type="text" class="form-control" id="otp_login" name="otp_login" placeholder="One Time Password" required="required">   
          </div> 
          <div class="form-group">
            <button type="button" id="otp_login_button" class="btn btn-primary btn-lg btn-block login-btn">Active </button>
          </div>          
        </form>
      </div>
      <div class="modal-footer">
        <!-- <a href="#">Forgot Password?</a> -->
      </div>
    </div>
  </div>
</div>

<!-- <div id="register_modal1" class="modal fade">
  <div style="width: 350px" class="modal-dialog modal-login">
    <div class="modal-content">
      <div class="modal-header">
        <div class="avatar">
          <img src="<?php echo base_url('assets/img/fevicon.jpg')?>" alt="Avatar">
        </div>        
        <h4 class="modal-title">Member Registration</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <form action="" id="register_form" method="post">
          <div class="form-group">
            <input type="text" class="form-control" id="register_username" name="register_username" placeholder="Mobile NO" required="required">   
          </div>
           <div class="form-group">
            <input type="text" class="form-control" name="register_nic" placeholder="NIC NO" >   
          </div>
          <div class="form-group">
            <input type="password" class="form-control" id="register_password" name="register_password" placeholder="Password" required="required"> 
          </div>  
            <div class="form-group">
            <input type="password" class="form-control" id="register_cnpassword" name="register_cnpassword" placeholder=" Retype Password" required="required"> 
          </div>        
          <div class="form-group">
            <button type="button" id="next_btn" class="btn btn-primary btn-lg btn-block login-btn">Next</button>
           
          </div>
        </form>
        <form action="" id="otp_form" method="post">
           <div class="form-group">
            <input type="text" class="form-control" id="otp" name="otp" placeholder="One Time Password" required="required">   
          </div> 
          <div class="form-group">
            <button type="button" id="register_btn" class="btn btn-primary btn-lg btn-block login-btn">Register</button>
          </div>          
        </form>
      </div>
      <div class="modal-footer">
        <!-- <a href="#">Forgot Password?</a> -->
      </div>
    </div>
  </div>
</div> -->
  <script src="<?php echo base_url('assets/js/vendor/jquery-1.12.4.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.validate.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.toast.js')?>"></script>
<script type="text/javascript">
 // $(document).ready(function() {

 //  $('#otp_form').hide();
 //    $("#register_form").validate({
 //      rules: {

 //        register_username: {
 //          required: true,
 //        },
     
 //        register_password: {
 //          required:true,    
 //        },
 //        register_cnpassword: {
 //          required:true,  
 //          equalTo: "#register_password"  
 //        },

 //      },
 //      messages: {


 //      }
 //      , errorElement: "div",
 //      errorPlacement: function (error, element) {
 //        error.insertAfter(element);
 //      }
 //    });

 //       $("#otp_form").validate({
 //      rules: {

 //        otp: {
 //          required: true,
          
 //        },
     

 //      },
 //      messages: {

      
 //      }
 //      , errorElement: "div",
 //      errorPlacement: function (error, element) {
 //        error.insertAfter(element);
 //      }
 //    });


 //         $("#login_form").validate({
 //           rules: {

 //             lgn_username: {
 //               required: true,
 //             },
          
 //             lgn_password: {
 //               required:true,    
 //             },
         

 //           },
 //           messages: {


 //           }
 //           , errorElement: "div",
 //           errorPlacement: function (error, element) {
 //             error.insertAfter(element);
 //           }
 //         });

 //            $("#otp_form").validate({
 //           rules: {

 //             otp: {
 //               required: true,
               
 //             },
          

 //           },
 //           messages: {

 //             otp: {
 //                     remote: "OTP Does not match"
 //                  }
 //           }
 //           , errorElement: "div",
 //           errorPlacement: function (error, element) {
 //             error.insertAfter(element);
 //           }
 //         });

 //  });



 //    $('#next_btn').on('click', function() {
    
 //      if($("#register_form").valid()){
 //        $('#register_form').hide();
 //        $('#otp_form').show();
 //        username = $('#register_username').val();
  
 //    $.ajax({
 //      type : 'POST',
 //      url : "<?php echo base_url('Home/register'); ?>" ,
 //      data : $('#register_form').serialize(),

 //      success: function(res){

 //        if(res == "true"){
 //          $("#register_form")[0].reset();


 //        }

 //      }
 //    });
 //  }


 // });

    // $('#register_btn').on('click', function() {
    //   if($("#otp_form").valid()){
    //     $.ajax({
    //       type : 'POST',
    //       url : "<?php echo base_url('Home/otp_validate'); ?>" ,
    //       data : {otp:$('#otp').val(), username: username},

    //       success: function(res){
          
    //         if(res == 'true'){
    //           $("#otp_form")[0].reset();
    //           $("#register_modal1").hide();

    //           $.toast({
    //                heading: 'Success',
    //                text: 'Login successfull',
    //                showHideTransition: 'plain',
    //                hideAfter: 2000,
    //                position: 'top-right',
    //                bgColor: '#007bff ',
    //                icon: 'success'
    //              });

    //         }

    //       }
    //     });
    //   }

    // })


    // $('#login_btn').on('click', function() {
    //   if($("#login_form").valid()){
    //     $.ajax({
    //       type : 'POST',
    //       url : "<?php echo base_url('Home/user_auth'); ?>" ,
    //       data : $('#login_form').serialize(),
    //       dataType: 'json',
    //       success: function(res){
                  
    //         if(res == 'success'){
    //           $("#login_modal").hide();

    //           $.toast({
    //                heading: 'Success',
    //                text: 'Login successfull',
    //                showHideTransition: 'plain',
    //                hideAfter: 2000,
    //                position: 'top-right',
    //                bgColor: '#007bff ',
    //                icon: 'success'
    //              });
    //            location.reload();
    //         }if(res == 'verify_account'){
    //           $.toast({
    //                heading: 'Warning',
    //                text: 'Please Verify Your Account',
    //                showHideTransition: 'plain',
    //                hideAfter: 4000,
    //                position: 'top-right',
    //                bgColor: '#ffc107  ',
    //                icon: 'warning'
    //              });
    //           $('#active_btn').show();
    //         }if(res== 'invalid'){
    //          $.toast({
    //               heading: 'Danger',
    //               text: 'Invalid Username or Password',
    //               showHideTransition: 'plain',
    //               hideAfter: 4000,
    //               position: 'top-right',
    //               bgColor: '#dc3545   ',
    //               icon: 'danger'
    //             }); 
    //         }
            
    //       }
    //     });
    //   }

    // })

// $('#active_btn').click(function(){

//         $('#login_form').hide();
//         $('#otp_form_login').show();
//          $('#active_btn').hide();
        
// });


// $('#otp_login_button').click(function(){

//         $('#login_form').show();
//         $('#otp_form_login').hide();
//          $('#active_btn').hide();
//         username = $('#lgn_username').val();
//         otp_login = $('#otp_login').val();
  
//     $.ajax({
//       type : 'POST',
//       url : "<?php echo base_url('Home/active_account'); ?>" ,
//       data : {username: username, otp_login: otp_login},

//       success: function(res){

//         if(res == "true"){
//           $("#login_form")[0].reset();


//         }

//       }
//     });

// });

</script>