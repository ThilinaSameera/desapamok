<!doctype html>
<!--
Website By Miradontsoa / MiVFX
http://twitter.com/miradontsoa
http://miradontsoa.com
-->

<html class="no-js" lang="en">


<?php require_once(APPPATH . 'views/template/css.php'); ?>
<script src="<?php echo base_url('assets/js/vendor/modernizr-2.7.1.min.js')?>"></script>

<body id="menu" class="body-page">
  <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

  <!-- Page Loader : just comment these lines to remove it -->
  <div class="page-loader" id="page-loader">
    <div>
      <div class="icon ion-spin"></div>
      <p>loading</p>
    </div>
  </div>

  <!-- BEGIN OF site header Menu -->
<?php require_once(APPPATH . 'views/template/header.php'); ?>
  
  <!-- END OF site header Menu-->

  <!-- BEGIN OF page cover -->
  <div class="hh-cover page-cover">
    <!-- Cover Background -->
    <div class="cover-bg bg-img " data-image-src="<?php echo base_url('assets/img/bg-default1.jpg')?>"></div>

    <!-- Particles as background - uncomment below to use particles/snow -->
    <div id="particles-js" class="cover-bg pos-abs full-size bg-color" data-bgcolor="rgba(2, 3, 10, 0.7)"></div>
  </div>
  <!--END OF page cover -->

  <!-- BEGIN OF page main content -->
  <main class="page-main page-fullpage main-anim" id="mainpage">

    <!-- Begin of home section -->
    <div class="section section-home fullscreen-md fp-auto-height-responsive " data-section="home">
      <!-- Begin of section wrapper -->
      <div class="section-wrapper">
        <!-- content -->
        <div class="section-content anim">
          <!-- <div class="row"> -->
            <?php require_once(APPPATH . 'views/template/count.php'); ?>
            
           <!--  <div class="col-12 col-md-6 text-left">
             
              <div class="title-desc">
                <h2 class="display-4 display-title home-title bordered anim-1">Simpleux</h2>
                <h4 class="anim-2">A Bootstrap 4 based template, helping you to build modern and beautiful
                  websites.</h4>
              </div>

           
              <div class="btns-action anim-3">
                <a class="btn btn-outline-white btn-round" href="#about">
                  Get started
                </a>
              </div>
            </div> -->

            <!-- begin of right content -->
            <div class="col-12 col-md-6 right-content hidden-sm center-vh">
              <!-- content -->
              <div class="section-content">
                <!-- illustartion -->
                <div class="illustr zoomout-1">
                  <!-- <img class="logo" src="<?php echo base_url('assets/img/disapamok.jpg')?>" alt="Logo"> -->
                </div>
              </div>
            </div>
            <!-- end of right content -->
          </div>
        </div>


        <!-- Arrows scroll down/up -->
        <footer class="section-footer scrolldown">
          <a class="down">
            <span class="icon"></span>
            <span class="txt">Scroll Down</span>
          </a>
        </footer>
      </div>
      <!-- End of section wrapper -->
    </div>
    <!-- End of home section -->
  <?php require_once(APPPATH . 'views/template/about.php'); ?>
    
      <!-- End of section wrapper -->
    </div>
    <!-- End of description section -->
<?php// require_once(APPPATH . 'views/template/service.php'); ?>
    <!-- Begin of list feature section -->
    
    <!-- Begin of slider section -->
    
    <!-- End of list two side 1 section -->
<?php require_once(APPPATH . 'views/template/project.php'); ?>
    <!-- Begin of list two side 1 section -->
    
    <!-- End of list two side 1 section -->


    <!-- Begin of register/login/signin section -->
    <?php require_once(APPPATH . 'views/template/subcribe.php'); ?>
    <!-- End of register/login/signin section -->

    <!-- Begin of contact section -->
  <?php require_once(APPPATH . 'views/template/contact.php'); ?>
    <!-- End of contact section -->
  </main>
  <!-- END OF page main content -->

  <!-- BEGIN OF page footer -->
  <footer id="site-footer" class="page-footer">
    <!-- Left content -->
    <div class="footer-left">
      <p>PORTFOLIO BY
        <a href="http://highhay.com">
          <span class="marked">BRAND</span>
        </a>
      </p>
    </div>

    <!-- Right content -->
    <div class="footer-right">
      <ul class="social">
        <li>
          <a href="">
            <i class="icon fa fa-facebook"></i>
          </a>
        </li>
        <li>
          <a href="">
            <i class="icon fa fa-twitter"></i>
          </a>
        </li>
        <li>
          <a href="">
            <i class="icon fa fa-linkedin"></i>
          </a>
        </li>
        <li>
          <a href="">
            <i class="icon fa fa-instagram"></i>
          </a>
        </li>
      </ul>
    </div>
  </footer>
  <!-- END OF site footer -->

  <!-- scripts -->
  <!-- All Javascript plugins goes here -->
  <script src="<?php echo base_url('assets/js/vendor/jquery-1.12.4.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.validate.min.js')?>"></script>

  <!-- All vendor scripts -->
  <script src="<?php echo base_url('assets/js/vendor/scrolloverflow.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/vendor/all.js')?>"></script>
  <script src="<?php echo base_url('assets/js/particlejs/particles.min.js')?>"></script>

  <!-- Countdown script -->
  <script src="<?php echo base_url('assets/js/jquery.downCount.js')?>"></script>

  <!-- Form script -->
  <script src="<?php echo base_url('assets/js/form_script.js')?>"></script>

  <!-- Javascript main files -->
  <script src="<?php echo base_url('assets/js/main.js')?>"></script>

  <script src="<?php echo base_url('assets/js/particlejs/particles-init.js')?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.toast.js')?>"></script>

</body>

</html>