
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/count/vendor/bootstrap/css/bootstrap.min.css')?>"> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/count/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>"> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/count/vendor/animate/animate.css')?>"> -->
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/count/vendor/select2/select2.min.css')?>"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/count/css/util.css')?>">
	
<!--===============================================================================================-->
<div class="row">
	<div class="size1 overlay1">
		<div class="section section-list-feature fp-auto-height-responsive " data-section="services">
			<div class="section-wrapper twoside anim" id="sub-list-wrap">
				<div class="content bg-grad gradient" id="background-gradiend">
					
								<?php if(!empty($subjects)){
                    foreach ($subjects as $subject) {?>
                      <button class="button glass blue-a" type="button"><b><?php echo $subject->subject;?></b></button>
          <?php }
          }?>
					
				</div>
			</div>
		</div>
	</div>
	<div class="size2 overlay1 load-registration">
		<div class="size2 flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-50">
			<div class="login-border">
				<div class="form-gro id="background-gradiend"up">
            		<a href="#" id="active_btn" class="" style="display: none;">Active Account</a>
        		</div>
        	<form action="" id="login_form" method="post">
          		<div class="form-group">
            		<input type="text" class="form-control" name="lgn_username" placeholder="Username" required="required" id="lgn_username">   
          		</div>
         	 	<div class="form-group">
            		<input type="password" class="form-control" name="lgn_password" placeholder="Password" required="required"> 
          		</div>        
          		<div class=" row form-group">
            		<button type="button" id="login_btn" class="btn btn-primary btn-lg btn-block ">Log In</button>
          		</div>
          		<div class=" row form-group">
                &nbsp;&nbsp;&nbsp;&nbsp;Still not a Member ? <a id="reg-modal-button" href="#register_modal" id="data-toggle="modal" target="_blank">&nbsp; Sign Up </a>
              </div>
        	</form>
        	<form action="" id="otp_form_login" method="post" style="display: none;">
           		<div class="form-group">
            		<input type="text" class="form-control" id="otp_login" name="otp_login" placeholder="One Time Password" required="required">   
          		</div> 
          		<div class="form-group">
            		<button type="button" id="otp_login_button" class="btn btn-primary btn-lg btn-block login-btn">Active </button>
          		</div>          
        	</form>
			</div>	
		</div>
	</div>
</div>


<div id="register_modal" class="modal fade">
  <div style="width: 350px" class="modal-dialog modal-login">
    <div class="modal-content">
      <div class="modal-header">
        <div class="avatar">
          <img src="<?php echo base_url('assets/img/fevicon.jpg')?>" alt="Avatar">
        </div>        
        <h4 class="modal-title">Member Registration</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <form action="" id="register_form" method="post">
          <div class="form-group">
            <input type="text" class="form-control" id="register_username" name="register_username" placeholder="Mobile NO" required="required">   
          </div>
           <div class="form-group">
            <input type="text" class="form-control" name="register_nic" placeholder="NIC NO" >   
          </div>
          <div class="form-group">
            <input type="password" class="form-control" id="register_password" name="register_password" placeholder="Password" required="required"> 
          </div>  
            <div class="form-group">
            <input type="password" class="form-control" id="register_cnpassword" name="register_cnpassword" placeholder=" Retype Password" required="required"> 
          </div>        
          <div class="form-group">
            <button type="button" id="next_btn" class="btn btn-primary btn-lg btn-block login-btn">Next</button>
           
          </div>
        </form>
        <form action="" id="otp_form" method="post">
           <div class="form-group">
            <input type="text" class="form-control" id="otp" name="otp" placeholder="One Time Password" required="required">   
          </div> 
          <div class="form-group">
            <button type="button" id="register_btn" class="btn btn-primary btn-lg btn-block login-btn">Register</button>
          </div>          
        </form>
      </div>
      <div class="modal-footer">
        <!-- <a href="#">Forgot Password?</a> -->
      </div>
    </div>
  </div>
</div>	

<!--===============================================================================================-->	
	<script src="<?php echo base_url('assets/count/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/count/vendor/bootstrap/js/popper.js')?>"></script>
	<script src="<?php echo base_url('assets/count/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/count/vendor/select2/select2.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/count/vendor/countdowntime/moment.min.js')?>"></script>
	<script src="<?php echo base_url('assets/count/vendor/countdowntime/moment-timezone.min.js')?>"></script>
	<script src="<?php echo base_url('assets/count/vendor/countdowntime/moment-timezone-with-data.min.js')?>"></script>
	<script src="<?php echo base_url('assets/count/vendor/countdowntime/countdowntime.js')?>"></script>
	<script>
		$('.cd100').countdown100({
			/*Set Endtime here*/
			/*Endtime must be > current time*/
			endtimeYear: 2019,
			endtimeMonth: 12,
			endtimeDate: 30,
			endtimeHours: 24,
			endtimeMinutes: 60,
			endtimeSeconds: 60,
			timeZone: ""  
			// ex:  timeZone: "America/New_York"
			//go to " http://momentjs.com/timezone/ " to get timezone
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/count/vendor/tilt/tilt.jquery.min.js')?>"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/count/js/main.js')?>"></script>

<script type="text/javascript">
	$('#sign_btn').click(function(){
		$.post( "<?php echo base_url();?>Home/view_register_model", function( data ) {
			if(msg){
				$('.load-registration').html(msg);
			}			
		});
	});
</script>

  <script type="text/javascript">
  $('#otp_form').hide();

   $('#next_btn').on('click', function() {
    
      if($("#register_form").valid()){
        $('#register_form').hide();
        $('#otp_form').show();
        username = $('#register_username').val();
  
    $.ajax({
      type : 'POST',
      url : "<?php echo base_url('Home/register'); ?>" ,
      data : $('#register_form').serialize(),

      success: function(res){

        if(res == "true"){
          $("#register_form")[0].reset();


        }

      }
    });
  }


 });



    $('#register_btn').on('click', function() {
      if($("#otp_form").valid()){
        $.ajax({
          type : 'POST',
          url : "<?php echo base_url('Home/otp_validate'); ?>" ,
          data : {otp:$('#otp').val(), username: username},

          success: function(res){
          
            if(res == 'true'){
              $("#otp_form")[0].reset();
              $("#register_modal_new").hide();

              $.toast({
                   heading: 'Success',
                   text: 'Login successfull',
                   showHideTransition: 'plain',
                   hideAfter: 2000,
                   position: 'top-right',
                   bgColor: '#007bff ',
                   icon: 'success'
                 });

            }

          }
        });
      }

    });












</script>