<div class="section section-twoside fp-auto-height-responsive " data-section="projects">
  <!-- Begin of section wrapper -->
  <div class="section-wrapper twoside">
    <!-- title -->
    <div class="section-title text-center">
      <h5 class="title-bg ">Projects</h5>
    </div>


    <!-- begin of carousel-swiper-beta -->
    <div class="slider-wrapper carousel-swiper-beta carousel-smalls carousel-swiper-beta-demo mt-40">
      <!-- slider -->
      <div class="slider-container swiper-container">
        <ul class="item-list swiper-wrapper">
          <!-- item -->
          <li class="slide-item swiper-slide">
            <div class="item-wrapper">
              <div class="illustr">
                <img src="<?php echo base_url('assets/img/items/img-sample2-small.jpg')?>" alt="Image" class="img">
              </div>
              <a class="legend" href="item.html#project_url">
                <h3 class="display-3">Mountain</h3>
                <h4>Music / Beats</h4>
              </a>
            </div>
          </li>
          <!-- item -->
          <li class="slide-item swiper-slide">
            <div class="item-wrapper">
              <div class="illustr">
                <img src="<?php echo base_url('assets/img/items/img-sample3-small.jpg')?>" alt="Image" class="img">
              </div>
              <a class="legend" href="item.html#project_url">
                <h3 class="display-3">Cloud</h3>
                <h4>Music / Beats</h4>
              </a>
            </div>
          </li>
          <!-- item -->
          <li class="slide-item swiper-slide">
            <div class="item-wrapper">
              <div class="illustr">
                <img src="<?php echo base_url('assets/img/items/img-sample4-small.jpg')?>" alt="Image" class="img">
              </div>
              <a class="legend" href="item.html#project_url">
                <h3 class="display-3">Garden</h3>
                <h4>Music / Beats</h4>
              </a>
            </div>
          </li>
          <!-- item -->
          <li class="slide-item swiper-slide">
            <div class="item-wrapper">
              <div class="illustr">
                <img src="<?php echo base_url('assets/img/items/img-sample5-small.jpg')?>" alt="Image" class="img">
              </div>
              <a class="legend" href="item.html#project_url">
                <h3 class="display-3">Sky</h3>
                <h4>Music / Beats</h4>
              </a>
            </div>
          </li>
        </ul>
      </div>
      <!-- pagination -->
      <div class="items-pagination bar"></div>

      <!-- navigation -->
      <div class="items-button bottom fit items-button-prev">
        <a class="btn btn-transp-arrow btn-primary icon-left" href="">
          <span class="icon arrow-left"></span>
          <span class="text">Prev</span>
        </a>
      </div>
      <div class="items-button bottom fit items-button-next">
        <a class="btn btn-transp-arrow btn-primary" href="">
          <span class="icon arrow-right"></span>
          <span class="text">Next</span>
        </a>
      </div>
    </div>
    <!-- end of carousel-swiper-beta -->


  </div>
  <!-- End of section wrapper -->
</div>
<!-- End of slider section -->

<!-- Begin of list two side 1 section -->
<div class="section section-twoside fp-auto-height-responsive " data-section="products">
  <!-- Begin of section wrapper -->
  <div class="section-wrapper twoside">

    <!-- title -->
    <div class="section-title text-center ">
      <h5 class="title-bg">Products</h5>
      <div class="title-abs">
        <h2 class="display-4 display-title">Products</h2>
        <p>Here is the list of our awesome pages.</p>
      </div>
    </div>

    <!-- text or illustration order are manipulated via Bootstrap order-md-1, order-md-2 class -->
    <!-- begin of item -->
    <div class="item row justify-content-between">
      <!-- img-frame-normal demo -->
      <div class="col-12 col-sm-6 col-md-4 center-vh">
        <div class="section-content anim translateUp">
          <div class="images text-center">
            <div class="img-frame-normal">
              <div class="img-1 shadow">
                <a href="index.html">
                  <img class="img" src="demopage/default.jpg" alt="Image">
                </a>
              </div>
              <div class="legend text-left pos-abs">
                <h5>Default</h5>
                <p class="small">Image or solid color as background of the page</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- img-frame-normal demo -->
      <div class="col-12 col-sm-6 col-md-4 center-vh">
        <div class="section-content anim">
          <div class="images text-center">
            <div class="img-frame-normal">
              <div class="img-1 shadow">
                <a href="index-slideshow.html">
                  <img class="img" src="demopage/slideshow.jpg" alt="Image">
                </a>
              </div>
              <div class="legend text-left pos-abs">
                <h5>Slideshow</h5>
                <p class="small">Fullscreen background image slideshow</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- img-frame-normal demo -->
      <div class="col-12 col-sm-6 col-md-4 center-vh">
        <div class="section-content anim translateDown">
          <div class="images text-center">
            <div class="img-frame-normal">
              <div class="img-1 shadow">
                <a href="index-video.html">
                  <img class="img" src="demopage/video.jpg" alt="Image">
                </a>
              </div>
              <div class="legend text-left pos-abs">
                <h5>Video</h5>
                <p class="small">Fullscreen video background</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- end of item -->
  </div>
  <!-- End of section wrapper -->
</div>
<!-- End of list two side 1 section -->

<!-- Begin of list two side 1 section -->
<div class="section section-twoside fp-auto-height-responsive " data-section="products-2">
  <!-- Begin of section wrapper -->
  <div class="section-wrapper twoside">

    <!-- title -->
    <div class="section-title text-center">
      <h5 class="title-bg">Products</h5>
    </div>

    <!-- text or illustration order are manipulated via Bootstrap order-md-1, order-md-2 class -->
    <!-- begin of item -->
    <div class="item row justify-content-between">
      <!-- img-frame-normal demo -->
      <div class="col-12 col-md-6 center-vh start-h">
        <div class="section-content anim translateUp">
          <div class="images text-center">
            <div class="img-frame-normal">
              <div class="img-1 shadow">
                <a href="item.html#project_url">
                  <img class="img" src="<?php echo base_url('assets/img/items/img-sample7.jpg')?>" alt="Image">
                </a>
              </div>
              <div class="legend text-left pos-abs">
                <h5>Computer Desktop</h5>
                <p class="small">Art / Programing</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- img-frame-normal demo, portrait orientation -->
      <div class="col-12 col-sm-6 col-md-5 col-lg-4 center-vh end-h">
        <div class="section-content anim">
          <div class="images text-center">
            <div class="img-frame-normal portrait">
              <div class="img-1 shadow">
                <a href="item.html#project_url">
                  <img class="img" src="<?php echo base_url('assets/img/items/img-portrait.jpg')?>" alt="Image">
                </a>
              </div>
              <div class="legend text-right pos-abs">
                <h5>Computer Desktop</h5>
                <p class="small">Art / Programing</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- end of item -->
  </div>
  <!-- End of section wrapper -->
</div>
<!-- End of list two side 1 section -->

<!-- Begin of list two side 1 section -->
<div class="section section-twoside fp-auto-height-responsive " data-section="product-item-alpha">
  <!-- Begin of section wrapper -->
  <div class="section-wrapper twoside center-vh dir-col">
    <!-- title -->
    <div class="section-title text-center">
      <h5 class="title-bg">Interior</h5>
    </div>


    <!-- text or illustration order are manipulated via Bootstrap order-md-1, order-md-2 class -->

    <div class="item row justify-content-between">

      <!-- img-frame-legend-alpha demo -->
      <div class="col-12 col-sm-6 center-vh">
        <div class="section-content anim translateUp">
          <div class="images text-center">
            <div class="img-frame-legend-alpha">
              <div class="img-1 shadow">
                <img class="img" src="<?php echo base_url('assets/img/items/img-sample7.jpg')?>" alt="Image">
              </div>
              <div class="legend">
                <h2 class="display-4">
                  <strong>Computer Desktop</strong>
                </h2>
                <p class="">Art / Interior Design</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- img-frame-legend-alpha portrait mode bottom demo -->
      <div class="col-12 col-sm-6 col-md-5">
        <div class="section-content anim">
          <!-- title and description -->
          <div class="title-desc">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet
              malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum
              ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id
              pretium leo. Pellentesque luctus massa non elit viverra pellentesque.
              Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui.
              Fusce in urna sem.</p>
          </div>

          <a class="btn btn-transp-arrow btn-outline btn-primary btn-round" href="item.html#project_url">
            <span class="icon arrow-right"></span>
            <span class="text">View</span>
          </a>
        </div>
      </div>


    </div>
    <!-- end of item -->
  </div>
  <!-- End of section wrapper -->
</div>
<!-- End of list two side 1 section -->

<!-- Begin of list two side 1 section -->
<div class="section section-twoside fp-auto-height-responsive " data-section="product-item-beta">
  <!-- Begin of section wrapper -->
  <div class="section-wrapper twoside center-vh dir-col">
    <!-- title -->
    <div class="section-title text-center">
      <h5 class="title-bg">Photography</h5>
    </div>


    <!-- text or illustration order are manipulated via Bootstrap order-md-1, order-md-2 class -->

    <div class="item row justify-content-between">

      <!-- img-frame-legend-alpha demo -->
      <div class="col-12 col-sm-6 center-vh">
        <div class="section-content anim translateUp">
          <div class="images text-center">
            <div class="img-frame-legend-alpha">
              <div class="img-1 shadow">
                <img class="img" src="<?php echo base_url('assets/img/items/img-sample2.jpg')?>" alt="Image">
              </div>
              <div class="legend">
                <h2 class="display-4">
                  <strong>Expedition</strong>
                </h2>
                <p class="">Photo / Campaign</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- img-frame-legend-alpha portrait mode bottom demo -->
      <div class="col-12 col-sm-6 col-md-5">
        <div class="section-content anim">
          <!-- title and description -->
          <div class="title-desc">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet
              malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum
              ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id
              pretium leo. Pellentesque luctus massa non elit viverra pellentesque.
              Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui.
              Fusce in urna sem.</p>
          </div>

          <a class="btn btn-transp-arrow btn-outline btn-primary btn-round" href="item.html#project_url">
            <span class="icon arrow-right"></span>
            <span class="text">View</span>
          </a>
        </div>
      </div>


    </div>
    <!-- end of item -->
  </div>
  <!-- End of section wrapper -->
</div>
<!-- End of list two side 1 section -->