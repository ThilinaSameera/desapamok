<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct() {
	    parent:: __construct();
	    $this->load->model('Register_model');
	      
	    $this->load->helper(array('url', 'form', 'date'));
	    $this->load->library('session');

	    $this->load->model('Subject_model');
	       
	       
	 }
	public function index(){
		$subject_model = new Subject_model();

		$data['subjects'] = $subject_model->get_all_subjects();
		$this->load->view('template/home',$data);
	}


	public function register(){
		
		$data['mobile'] = $this->input->post('register_username');
		$data['NIC'] = $this->input->post('register_nic');
		$data['password'] = sha1($this->input->post('register_password'));
		$digits = RANDOM_NO;
        $OTP = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
		$data['OTP'] = $OTP;
		$insert_id =$this->Register_model->insert_student_data($data);

		if($insert_id > ZERO){
			echo json_encode(true);
		}else{
			echo json_encode(false);

		}
	}

	public function otp_validate(){
		$otp = $this->input->post('otp');
		$username = $this->input->post('username');

		$result = $this->Register_model->otp_validate($otp,$username);
		if($result == 'true'){
			$this->session->set_userdata('USER_TYPE',STUDENT);
			$this->session->set_userdata('USERNAME',$username);
		}
		echo json_encode($result);
	}
	

	public function user_auth(){
		$username=$this->input->post('lgn_username');
		$password = $this->input->post('lgn_password');

		$result = $this->Register_model->select_student_by_username($username,sha1($password));

	if(!empty($result)){
		if($result['is_active'] == ACTIVE){
			$this->session->set_userdata('USER_TYPE',STUDENT);
			$this->session->set_userdata('USERNAME',$username);
			echo json_encode("success");
		}else{
			echo json_encode("verify_account");
		}
	
	}else{
		echo json_encode("invalid");
	}

	}

	public function active_account(){
		$user_name = $this->input->post('username');
		$otp = $this->input->post('otp_login');

		$result = $this->Register_model->otp_validate($otp, $user_name);
		if($result == 'true'){
			$this->session->set_userdata('USER_TYPE',STUDENT);
			$this->session->set_userdata('USERNAME',$username);
		}
		echo json_encode($result);

	}

	public function view_register_model(){
		$this->load->view('template/register_form');
	}

}